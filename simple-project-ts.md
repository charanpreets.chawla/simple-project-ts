Author: Charanpreet Singh Chawla \
Title: Software Developer \
Date: 08-09-22 \

## Simple-Project
---
This is a simple project that demonstartes the use of **styled-components** in react.

#### ToDo 
- install styled-components.
- style components **styled**.
- media queries with **styled**.
- theme with context.
- animations and transitions with the same.

