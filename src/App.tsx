import { useState, useEffect } from 'react';
import './App.css';

import Display from './components/Display';
import Keypad from './components/Keypad';

// interfaces
import { keypadState } from './interfaces/interfaces';

function App() {

  const [resultStack, setResultStack] = useState<Array<number>>([]);
  const [keypadState, setKeypadState] = useState<keypadState>({
    isKeyPressed: false,
    pressedKey: ''
  });

  const computeImmediate = () => {
    let opr1 = resultStack[resultStack.length];
    let opr2 = resultStack[resultStack.length - 1];
    
    if (typeof(keypadState.pressedKey) == 'number') {
      setResultStack([...resultStack.filter((_, idx) => idx == resultStack.length ? false : true), resultStack[resultStack.length] * 10 + keypadState.pressedKey])
    } else {
      switch(keypadState.pressedKey){
        case '+':
          setResultStack([...resultStack.filter((_, idx) => idx > resultStack.length - 2 ? false : true), opr2 + opr1])
        case '-':
          setResultStack([...resultStack.filter((_, idx) => idx > resultStack.length - 2 ? false : true), opr2 - opr1])
        case '*':
          setResultStack([...resultStack.filter((_, idx) => idx > resultStack.length - 2 ? false : true), opr2 * opr1])
        case '/':
          setResultStack([...resultStack.filter((_, idx) => idx > resultStack.length - 2 ? false : true), opr2 / opr1])
        case 'C':
          setResultStack([]);
        case '\u2190':
          setResultStack(resultStack.filter((_, idx) => idx == resultStack.length ? false : true))
        default: 
          setResultStack(resultStack);
      }
    }
    console.log(resultStack);
  }

  useEffect(() => {
    computeImmediate()
  }, [keypadState])

  return (
    <div className="App">
      <Display mainDisplay={ typeof resultStack[resultStack.length] === 'number' ? resultStack[resultStack.length].toString() : resultStack[resultStack.length]} secondaryDisplay={''}/>
      <Keypad mode={'normal'} setKeypadState={setKeypadState}/>
    </div>
  )
}

export default App
