import React from 'react';

export interface keypadState {
    isKeyPressed: Boolean,
    pressedKey: String | Number
}