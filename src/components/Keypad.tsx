import React from 'react';
import { useState } from 'react';

//interfaces
import { keypadState } from '../interfaces/interfaces';

import { 
    KeypadContainer,
    KeypadNumbers,
    KeypadOperations,
    NumberButton,
    OperationButton 
} from './ComponentsStyled';

interface KeypadProps {
    mode: String,
    setKeypadState: React.Dispatch<React.SetStateAction<keypadState>>
};

const Keypad = (props: React.PropsWithChildren<KeypadProps>): React.ReactElement => {

    const numArr = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    const opArr1 = ['+', '-', '*', '/']
    const opArr2 = ['C', '←', '=']   

    const handleDown: React.MouseEventHandler<HTMLButtonElement> = (e) => {
        props.setKeypadState({ 
            isKeyPressed: true,
            pressedKey: e.currentTarget.innerText
        })
    }

    const handleUp: React.MouseEventHandler<HTMLButtonElement> = (e) => {
        props.setKeypadState({
            isKeyPressed: false,
            pressedKey: ''
        })
    }

    return (
        <KeypadContainer>
            <KeypadNumbers>
                {
                    numArr.map(num => 
                        <NumberButton 
                            key={Math.floor(Math.random() * 5000)}
                            onMouseDown={handleDown}
                            onMouseUp={handleUp}>{num}</NumberButton>
                    )
                }
            </KeypadNumbers>
            <KeypadOperations>
                {
                    opArr1.map(op => 
                        <OperationButton 
                            key={Math.floor(Math.random() * 5000)} 
                            onMouseDown={handleDown}
                            onMouseUp={handleUp}>{op}</OperationButton>
                    )
                }
            </KeypadOperations>
            <KeypadOperations>
                {
                    opArr2.map(op => 
                    <OperationButton 
                        key={Math.floor(Math.random() * 5000)} 
                        onMouseDown={handleDown}
                        onMouseUp={handleUp}>{op}</OperationButton>
                    )
                }
            </KeypadOperations>
        </KeypadContainer>
    );
};

export default Keypad;