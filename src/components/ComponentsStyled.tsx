import styled from 'styled-components';

export const KeypadContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
    align-content: center;
    justify-content: center;
    height: 300px;
    width: 360px;
`;

export const KeypadNumbers = styled.div`
    display: flex;
    flex-wrap: wrap;
    align-content: center;
    justify-content: center;
    width: 200px;
    border: 1px solid orange;
    border-radius: 32px;
`

export const KeypadOperations = styled.div`
    display: flex;
    flex-wrap: wrap;
    align-content: center;
    justify-content: center;
    width: 70px;
`

export const NumberButton = styled.button`
    border-style: none;
    font-size: 1.5rem;
    margin: 1px;
    background-color: orange;
    height: 64px;
    width: 64px;
    border-radius: 32px;
    transition: 
        box-shadow 0.2s ease-out, 
        background-color 0.2s ease-out;
    &:hover {
        box-shadow: 0px 2px 5px orange;
        background-color: #e67e00;
    }
`;

export const OperationButton = styled.button`
    border-style: none;
    font-size: 1.5rem;
    margin: 1px;
    background-color: darkgrey;
    height: 64px;
    width: 64px;
    border-radius: 32px;
    border-radius: 32px;
    transition: 
        box-shadow 0.2s ease-out, 
        background-color 0.2s ease-out;
    &:hover {
        box-shadow: 0px 2px 5px gray;
        background-color: #565656;
    }
`;

export const DisplayContainer = styled.div`
    display: flex;
    background-color: #666666;
    align-items: center;
    justify-contents: center;
    flex-direction: column;
    width: 350px;
    height: 150px;
    border: 5px solid black;
    border-radius: 32px 32px 0px 0px;
    overflow: hidden;
`;

export const DisplayInput = styled.input`
    height: 50%;
    width: 100%;
    direction: rtl;
    outline: none;
    border-style: none;
    font-size: 1.5rem;
    background-color: 000000;
    opacity: 0.5;
    backdrop-filter: blur;
`;