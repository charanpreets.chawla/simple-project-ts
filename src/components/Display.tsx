import React from 'react';
import styled from 'styled-components';

import { 
    DisplayContainer,
    DisplayInput
} from './ComponentsStyled';

interface DisplayProps {
    mainDisplay: string | number,
    secondaryDisplay: string
}
const Display = (props: React.PropsWithChildren<DisplayProps>): React.ReactElement => {
    return (
        <DisplayContainer>
            <DisplayInput value={props.secondaryDisplay} readOnly />
            <DisplayInput value={ typeof props.mainDisplay == 'number' ? props.mainDisplay.toString() : props.mainDisplay } placeholder={"Use keypad to type..."} readOnly /> 
        </DisplayContainer>
    );
};

export default Display;